import pygame

from constants import CELL_SIZE


class RectCell(pygame.Rect):
    def __init__(self, left, top, row, col):
        super().__init__(left, top, CELL_SIZE, CELL_SIZE)
        self.row = row
        self.col = col


class Cell:
    def __init__(self, row, col, value, editable):
        self.row = row
        self.col = col
        self.value = value
        self.editable = editable

    def __repr__(self):
        return f'{self.__class__.__name__}({self.value})'


class Sudoku:
    def __init__(self, board):
        self.board = [
            [
                Cell(row, col, None if val == 0 else val, val == 0)
                for col, val in enumerate(row_vals)
            ]
            for row, row_vals in enumerate(board)
        ]

    def check_move(self, cell, num):
        for col in range(9):
            if self.board[cell.row][col].value == num and col != cell.col:
                return False

        for row in range(9):
            if self.board[row][cell.col].value == num and row != cell.row:
                return False

        start_row = cell.row // 3 * 3
        start_col = cell.col // 3 * 3
        for row in range(start_row, start_row + 3):
            for col in range(start_col, start_col + 3):
                if (
                    self.board[row][col].value == num
                    and (row, col) != (cell.row, cell.col)
                ):
                    return False

        return True

    def get_empty_cell(self):
        for row in range(9):
            for col in range(9):
                if self.board[row][col].value is None:
                    return self.board[row][col]

        return None

    def solve(self):
        cell = self.get_empty_cell()
        if cell is None:
            return True
        for val in range(1, 10):
            if not self.check_move(cell, val):
                continue
            cell.value = val
            if self.solve():
                return True
            cell.value = None
        return False

    def get_board(self):
        return [[cell.value for cell in row] for row in self.board]

    def test_solve(self):
        current_board = self.get_board()
        solvable = self.solve()
        for row, line in enumerate(self.board):
            for col, cell in enumerate(line):
                cell.value = current_board[row][col]
        return solvable

    def reset(self):
        for row in self.board:
            for cell in row:
                if cell.editable:
                    cell.value = None

    def __str__(self):
        board_str = ' -----------------------\n'
        for row, line in enumerate(self.board):
            board_str += '|'
            for col, cell in enumerate(line):
                val = '-' if cell.value is None else cell.value
                board_str += f' {val}'
                if (col + 1) % 3 == 0:
                    board_str += ' |'
            board_str += '\n'
            if (row + 1) % 3 == 0:
                board_str += '|-------|-------|-------|\n'
        board_str += ' -----------------------\n'
        return board_str
