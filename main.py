import pygame
import sys

from constants import *
from solver import Sudoku, RectCell

pygame.init()

screen = pygame.display.set_mode(SIZE)
pygame.display.set_caption('Sudoku')


class SudokuGame:

    def assign_reset_btn(self, active):
        return self.draw_button(
            WIDTH - BUFFER - BTN_BORDER * 2 - BTN_WIDTH,
            HEIGHT - BTN_HEIGHT - BTN_BORDER * 2 - BUFFER,
            BTN_WIDTH,
            BTN_HEIGHT,
            BTN_BORDER,
            ACTIVE_BTN if active is True else INACTIVE_BTN,
            BLACK,
            RESET
        )

    def assign_solve_btn(self, active):
        return self.draw_button(
            WIDTH - BUFFER * 2 - BTN_BORDER * 4 - BTN_WIDTH * 2,
            HEIGHT - BTN_HEIGHT - BTN_BORDER * 2 - BUFFER,
            BTN_WIDTH,
            BTN_HEIGHT,
            BTN_BORDER,
            ACTIVE_BTN if active is True else INACTIVE_BTN,
            BLACK,
            SOLVE
        )


    def create_cells(self):
        cells = [[] for _ in range(9)]

        # Set attributes for first RectCell
        row = 0
        col = 0
        left = BUFFER + GRID_SIZE_L
        top = BUFFER + GRID_SIZE_L

        while row < 9:
            while col < 9:
                cells[row].append(RectCell(left, top, row, col))

                # Update attributes for next RectCell
                left += CELL_SIZE + GRID_SIZE_S
                if col != 0 and (col + 1) % 3 == 0:
                    left = left + GRID_SIZE_L - GRID_SIZE_S
                col += 1

            # Update attributes for next RectCell
            top += CELL_SIZE + GRID_SIZE_S
            if row != 0 and (row + 1) % 3 == 0:
                top = top + GRID_SIZE_L - GRID_SIZE_S
            left = BUFFER + GRID_SIZE_L
            col = 0
            row += 1

        return cells

    def draw_grid(self):
        # Draw minor grid lines
        lines_drawn = 0
        pos = BUFFER + GRID_SIZE_L + CELL_SIZE
        while lines_drawn < 6:
            pygame.draw.line(screen, BLACK, (pos, BUFFER),
                             (pos, WIDTH - BUFFER - 1), GRID_SIZE_S)
            pygame.draw.line(screen, BLACK, (BUFFER, pos),
                             (WIDTH - BUFFER - 1, pos), GRID_SIZE_S)

            lines_drawn += 1

            # Update pos for next lines
            pos += CELL_SIZE + GRID_SIZE_S
            if lines_drawn % 2 == 0:
                pos += CELL_SIZE + GRID_SIZE_L

        # Draw major grid lines
        for pos in range(BUFFER + GRID_SIZE_L // 2, WIDTH, CELL_SIZE * 3 + GRID_SIZE_S * 2 + GRID_SIZE_L):
            pygame.draw.line(screen, BLACK, (pos, BUFFER),
                             (pos, WIDTH - BUFFER - 1), GRID_SIZE_L)
            pygame.draw.line(screen, BLACK, (BUFFER, pos),
                             (WIDTH - BUFFER - 1, pos), GRID_SIZE_L)

    def visual_solve(self, game, cells):
        cell = game.get_empty_cell()
        if not cell:
            return True

        # Check each possible move
        for val in range(1, 10):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
            cell.value = val

            # Outline cell being changed in red
            screen.fill(WHITE)
            self.draw_board(None, cells, game)
            cell_rect = cells[cell.row][cell.col]
            pygame.draw.rect(screen, RED, cell_rect, 5)
            pygame.display.update([cell_rect])

            # Check if the value is a valid move
            if not game.check_move(cell, val):
                cell.value = None
                continue

            # Check recursive calls
            screen.fill(WHITE)
            pygame.draw.rect(screen, GREEN, cell_rect, 5)
            self.draw_board(None, cells, game)
            pygame.display.update([cell_rect])
            if self.visual_solve(game, cells):
                return True
            cell.value = None

        # Unsuccessful
        screen.fill(WHITE)
        pygame.draw.rect(screen, WHITE, cell_rect, 5)
        self.draw_board(None, cells, game)
        pygame.display.update([cell_rect])
        return False

    def draw_button(self, left, top, WIDTH, HEIGHT, border, color, border_color, text):
        pygame.draw.rect(
            screen,
            border_color,
            (left, top, WIDTH + border * 2, HEIGHT + border * 2),
        )

        button = pygame.Rect(
            left + border,
            top + border,
            WIDTH,
            HEIGHT
        )
        pygame.draw.rect(screen, color, button)

        font = pygame.font.Font(None, 26)
        text = font.render(text, 1, BLACK)
        xpos, ypos = button.center
        textbox = text.get_rect(center=(xpos, ypos))
        screen.blit(text, textbox)

        return button

    def draw_board(self, active_cell, cells, game):
        self.draw_grid()
        if active_cell is not None:
            pygame.draw.rect(screen, GRAY, active_cell)

        font = pygame.font.Font(None, 32)

        # Fill in all cells with correct value
        for row in range(9):
            for col in range(9):
                if game.board[row][col].value is None:
                    continue

                if not game.board[row][col].editable:
                    font.underline = True
                    text = font.render(f'{game.board[row][col].value}', 1, BLACK)

                else:
                    font.underline = False
                    if game.check_move(game.board[row][col], game.board[row][col].value):
                        text = font.render(
                            f'{game.board[row][col].value}', 1, GREEN)
                    else:
                        text = font.render(
                            f'{game.board[row][col].value}', 1, RED)

                xpos, ypos = cells[row][col].center
                textbox = text.get_rect(center=(xpos, ypos))
                screen.blit(text, textbox)

    def check_sudoku(self, sudoku):
        if sudoku.get_empty_cell():
            raise ValueError('Game is not complete')

        row_sets = [set() for _ in range(9)]
        col_sets = [set() for _ in range(9)]
        box_sets = [set() for _ in range(9)]

        for row in range(9):
            for col in range(9):
                box = (row // 3) * 3 + col // 3
                value = sudoku.board[row][col].value

                if value in row_sets[row] or value in col_sets[col] or value in box_sets[box]:
                    return False

                row_sets[row].add(value)
                col_sets[col].add(value)
                box_sets[box].add(value)

        return True

    def run_game(self):
        easy = [
            [0, 0, 0, 9, 0, 0, 0, 3, 0],
            [3, 0, 6, 0, 2, 0, 0, 4, 0],
            [2, 0, 4, 0, 0, 3, 1, 0, 6],
            [0, 7, 0, 0, 5, 1, 0, 8, 0],
            [0, 3, 1, 0, 6, 0, 0, 5, 7],
            [5, 0, 9, 0, 0, 0, 6, 0, 0],
            [4, 1, 0, 0, 0, 2, 0, 7, 8],
            [7, 6, 3, 0, 0, 5, 4, 0, 0],
            [9, 2, 8, 0, 0, 4, 0, 0, 1]
        ]
        medium = [
            [5, 3, 0, 0, 7, 0, 0, 0, 0],
            [6, 0, 0, 1, 9, 5, 0, 4, 0],
            [0, 9, 8, 0, 0, 0, 0, 6, 0],
            [8, 0, 0, 0, 6, 0, 0, 0, 3],
            [4, 0, 0, 8, 0, 3, 0, 9, 1],
            [7, 0, 0, 0, 2, 0, 0, 0, 6],
            [0, 6, 0, 0, 0, 0, 2, 8, 0],
            [0, 0, 0, 4, 1, 9, 0, 0, 5],
            [0, 0, 0, 0, 8, 0, 0, 7, 9]
        ]
        hard = [
            [5, 3, 0, 0, 7, 0, 0, 0, 0],
            [6, 0, 0, 1, 9, 5, 0, 0, 0],
            [0, 9, 8, 0, 0, 0, 0, 6, 0],
            [8, 0, 0, 0, 6, 0, 0, 0, 3],
            [4, 0, 0, 8, 0, 3, 0, 0, 1],
            [7, 0, 0, 0, 2, 0, 0, 0, 6],
            [0, 6, 0, 0, 0, 0, 2, 8, 0],
            [0, 0, 0, 4, 1, 9, 0, 0, 5],
            [0, 0, 0, 0, 8, 0, 0, 7, 9]
        ]
        game = Sudoku(medium)
        cells = self.create_cells()
        active_cell = None
        solve_rect = pygame.Rect(
            BUFFER,
            HEIGHT - BTN_HEIGHT - BTN_BORDER * 2 - BUFFER,
            BTN_WIDTH + BTN_BORDER * 2,
            BTN_HEIGHT + BTN_BORDER * 2
        )

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONUP:
                    mouse_pos = pygame.mouse.get_pos()
                    if reset_btn.collidepoint(mouse_pos):
                        game.reset()
                    if solve_btn.collidepoint(mouse_pos):
                        screen.fill(WHITE)
                        active_cell = None
                        self.draw_board(active_cell, cells, game)
                        pygame.display.flip()
                        self.visual_solve(game, cells)

                    active_cell = None
                    for row in cells:
                        for cell in row:
                            if cell.collidepoint(mouse_pos):
                                active_cell = cell
                    if active_cell and not game.board[active_cell.row][active_cell.col].editable:
                        active_cell = None
                if event.type == pygame.KEYUP:
                    if active_cell is not None:

                        if event.unicode.isnumeric():
                            key_value = int(event.unicode)
                            game.board[active_cell.row][active_cell.col].value = key_value
                        if event.key == pygame.K_BACKSPACE:
                            game.board[active_cell.row][active_cell.col].value = None

            screen.fill(WHITE)

            self.draw_board(active_cell, cells, game)

            reset_btn = self.assign_reset_btn(False)
            solve_btn = self.assign_solve_btn(False)

            if reset_btn.collidepoint(pygame.mouse.get_pos()):
                reset_btn = self.assign_reset_btn(True)

            if solve_btn.collidepoint(pygame.mouse.get_pos()):
                solve_btn = self.assign_solve_btn(True)

            # Check if game is complete
            if not game.get_empty_cell():
                if self.check_sudoku(game):
                    font = pygame.font.Font(None, 32)
                    text = font.render('Solved!', 1, GREEN)
                    textbox = text.get_rect(center=(solve_rect.center))
                    screen.blit(text, textbox)

            pygame.display.flip()


if __name__ == '__main__':
    sudoku = SudokuGame()
    sudoku.run_game()
